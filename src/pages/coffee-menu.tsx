import {Flex, Heading, Link as ChakraLink, Button } from '@chakra-ui/react'
import { CoffeeMenuGrid } from '../components/CoffeeMenuGrid'
import { Container } from '../components/Container'
import { Main } from '../components/Main'
import{useState} from 'react'
import NextLink from "next/link";

const coffeeMenu = () => {
return (
    <Container height="100vh">
      <Main >
     <NextLink href="/" passHref>
     <ChakraLink as={Button}>Back home</ChakraLink>
     </NextLink>
      <Flex align='center' direction='column'>
        <Heading mb={4} size='xl'>Stijlbreuk's coffee menu</Heading>
      </Flex>
      <CoffeeMenuGrid></CoffeeMenuGrid>
      </Main>
    </Container>
  )
}


export default coffeeMenu