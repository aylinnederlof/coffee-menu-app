import {
  Link as ChakraLink,
  Text,
  Code,
  List,
  ListIcon,
  ListItem,
  Flex,
  Heading,
} from '@chakra-ui/react';
import NextLink from "next/link";
import { CheckCircleIcon, LinkIcon } from '@chakra-ui/icons'
import { Container } from '../components/Container'
import { Main } from '../components/Main'
import { Image } from '../components/Image'
// import { DarkModeSwitch } from '../components/DarkModeSwitch'


const Index = () => {


  return(
  <Container height="100vh" >
    <Main>
      <Flex align='center' direction='column'>
        <Heading mb={4} size='xl' >Stijlbreuk's coffee bar</Heading>
        <Image src='/images/general/stijlbreuk.jpeg' alt="descriptive" width="320px" height="213px"/>
        <NextLink href="/coffee-menu" passHref>
          <ChakraLink >To coffee menu</ChakraLink>
        </NextLink>
      </Flex>
      
    </Main>

    {/* <DarkModeSwitch /> */}

  </Container>
  )
}

export default Index
