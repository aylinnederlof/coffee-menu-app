import { AddIcon, DeleteIcon, MinusIcon } from '@chakra-ui/icons';
import { Box, Button, Flex, Heading, IconButton, Text } from '@chakra-ui/react'
import { useState } from 'react';
import { Coffee } from './CoffeeMenuGrid';
import { Image }  from './Image'
 

type CoffeeCardProps = {
  coffee: Coffee
  deleteCoffee: (coffeeId: string) => void
}
export const CoffeeCard = ({coffee, deleteCoffee}:CoffeeCardProps) => {
  // console.log(props);
    return(
      <Box className='coffeeCard' p={4} position='relative' bg='white' borderRadius='lg' border='2px' borderColor='orange.100'>
        <IconButton display='none' sx={{'.coffeeCard:hover &': { display:'block '}}} onClick={() => deleteCoffee(coffee.id)} _hover={{bg:'red.100'}} size='sm' position='absolute' right='1rem' aria-label='Add to friends' icon={<DeleteIcon />} />
        <Flex justify='center' >
          <Image src={coffee.imageUrl} alt={coffee.title} width="200px" height="200px"/>
        </Flex>
        <Heading size='md'>{coffee.title}</Heading>
        <Text> € {coffee.price}</Text>
      </Box>
    )
}
