import {
  Box,
  Button,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Grid,
  GridItem,
  Icon,
  Input,
  useDisclosure,
  Select,
  FormLabel,
  NumberInput,
  NumberInputField,
  FormControl,
  Checkbox,
  CheckboxGroup,
  Stack,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { CoffeeCard } from "./CoffeeCard";
import { v4 as uuidv4 } from "uuid";
import { AddIcon } from "@chakra-ui/icons";
import React from "react";

export type Coffee = {
  title: string;
  imageUrl: string;
  price: number;
  size?: string;
  id: string;
};

export const CoffeeMenuGrid = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  const [allCoffees, setAllCoffees] = useState<Coffee[]>([]);
  const [coffee, setCoffee] = useState<Coffee>({
    title: "",
    imageUrl: "",
    price: 0,
    id: "",
  });

  const addNewCoffee = (event) => {
    event.preventDefault();
    console.log(coffee);

    if (coffee.imageUrl === "") {
      coffee.imageUrl = "/images/coffeeTypes/Lungo.png";
    }
    const newCoffee = { ...coffee, id: uuidv4() };

    const newCoffees: Coffee[] = [...allCoffees, newCoffee];
    setAllCoffees(newCoffees);
    setCoffee({ title: "", imageUrl: "", price: 0, id: "" });

    onClose();
  };

  const deleteCoffee = (coffeeId: string) => {
    const newCoffees = allCoffees.filter((coffee) => coffee.id !== coffeeId);
    setAllCoffees(newCoffees);
  };

  return (
    <>
      <Box>
        <Button
          ref={btnRef}
          onClick={onOpen}
          colorScheme="green"
          variant="outline"
          bg="white"
          _hover={{ bg: "green.500", color: "white" }}
          border="2px"
          borderColor="green.500"
          rightIcon={<AddIcon color="currentColor" />}
        >
          Add New coffee
        </Button>
      </Box>

      {/* DRAWER */}
      <Drawer
        isOpen={isOpen}
        placement="right"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Add new coffee type</DrawerHeader>

          <DrawerBody>
            <form>
              <FormControl mb={4} isRequired>
                <FormLabel>Name</FormLabel>
                <Input
                  type="text"
                  value={coffee["title"]}
                  placeholder="Name..."
                  onChange={(e) =>
                    setCoffee((prev) => ({ ...prev, title: e.target.value }))
                  }
                />
              </FormControl>
              <FormControl mb={4} isRequired>
                <FormLabel htmlFor="coffeeImageSelect">Select icon</FormLabel>
                <Select
                  id="coffeeImageSelect"
                  placeholder="Select image"
                  onChange={(e) =>
                    setCoffee((prev) => ({
                      ...prev,
                      imageUrl:
                        "/images/coffeeTypes/" + e.target.value + ".png",
                    }))
                  }
                >
                  <option>Americano</option>
                  <option>Corretto</option>
                  <option>Espresso Romano</option>
                  <option>Espresso</option>
                  <option>Galao</option>
                  <option>Latte</option>
                  <option>Lungo</option>
                  <option>Macchiato</option>
                </Select>
              </FormControl>
              <FormControl mb={4} isRequired>
                <FormLabel>Enter price</FormLabel>
                <NumberInput
                  onChange={(number) =>
                    setCoffee((prev) => ({ ...prev, price: Number(number) }))
                  }
                >
                  <NumberInputField value={coffee["price"]} />
                </NumberInput>
              </FormControl>
            </form>
          </DrawerBody>

          <DrawerFooter>
            <Button variant="outline" mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button colorScheme="green" onClick={addNewCoffee}>
              Save
            </Button>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>

      {/* GRID */}
      <Grid templateColumns="repeat(3, 1fr)" gap={6}>
        {allCoffees.map((coffee) => {
          // console.log(coffee);
          // console.log(coffee.title);
          return (
            <GridItem key={coffee.id} w="100%" h="auto">
              <CoffeeCard
                deleteCoffee={deleteCoffee}
                coffee={coffee}
              ></CoffeeCard>
            </GridItem>
          );
        })}
      </Grid>
    </>
  );
};
function getAriaValueText(e: string) {
  throw new Error("Function not implemented.");
}
